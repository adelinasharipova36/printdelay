package com.example.delayprint

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        runBlocking {

            launch {
                delay(ONE_SECONDS_DELAY)
                println("World!")
            }

            delay(TWO_SECONDS_DELAY)
            println("Hello")

            for (i in 0..2) {
                println("I'm sleeping $i ...")
                delay(ONE_SECONDS_DELAY)
            }

            // По условию блокируем поток (на 1 сек)
            delay(ONE_SECONDS_DELAY)

            println("main: I'm tired of waiting!")
            println("main: I'm running finally")
            println("main: Now I can quit.")

            val startTimeFirstIteration = System.currentTimeMillis()
            val resultFirstIteration = firstDelayedFunction() + secondDelayedFunction()
            val endTimeFirstIteration = System.currentTimeMillis()
            val wastedTimeFirstIteration = endTimeFirstIteration - startTimeFirstIteration

            println("Полученная сумма: $resultFirstIteration, затраченное время: $wastedTimeFirstIteration")

            val startTimeSecondIteration = System.currentTimeMillis()
            val deferredFirstFunction = async { firstDelayedFunction() }
            val deferredSecondFunction = async { secondDelayedFunction() }
            val resultSecondIteration = deferredFirstFunction.await() + deferredSecondFunction.await()
            val endTimeSecondIteration = System.currentTimeMillis()
            val wastedTimeSecondIteration = endTimeSecondIteration - startTimeSecondIteration
            println("Полученная сумма: $resultSecondIteration, затраченное время: $wastedTimeSecondIteration")
        }
    }

    private suspend fun firstDelayedFunction(): Int {
        delay(ONE_AND_A_HALF_SECONDS_DELAY)
        return 1
    }

    private suspend fun secondDelayedFunction(): Int {
        delay(ONE_SECONDS_DELAY)
        return 2
    }

    private companion object {
        const val ONE_SECONDS_DELAY = 1000L
        const val ONE_AND_A_HALF_SECONDS_DELAY = 1500L
        const val TWO_SECONDS_DELAY = 2000L
    }
}